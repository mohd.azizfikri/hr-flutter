import 'dart:convert';

import 'package:hr_flutter/app/constants/api_path.dart';
import 'package:hr_flutter/app/modules/login/models/login.dart';
import 'package:http/http.dart' as http;

class LoginProvider {
  Future<Login> login({required Map dataLogin}) async {
    print('data login:: $dataLogin');

    var url = Uri.parse(ApiPath.loginUrl);
    var response = await http.post(url, body: dataLogin);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    return Login.fromJson(json.decode(response.body));
  }
}
