import 'package:hr_flutter/app/utils/services/local_storage.dart';

Map<String, String> headerApi() {
  return {'x-token': getToken().toString()};
}
