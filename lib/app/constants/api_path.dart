abstract class ApiPath {
  //Global
  static const baseUrl = 'https://demohr.soldig.co.id';

  //Pegawai
  static const basePegawaiUrl = baseUrl + 'hrapi/pegawai';

  //Authentication
  static const loginUrl = basePegawaiUrl + 'login';
  static const logoutUrl = basePegawaiUrl + 'logout';
  static const updateFCMUrl = basePegawaiUrl + 'update_fcm';

  //Absensi
  static const absenPegawaiUrl = basePegawaiUrl + 'absensi';
  static const listAbsensiUrl = basePegawaiUrl + 'list_absen';
  static const profilePegawaiUrl = basePegawaiUrl + 'profile';

  //Feature
  static const pengumumanUrl = basePegawaiUrl + 'pengumuman';

  static const pengajuanCutiUrl = basePegawaiUrl + 'pengajuan_cuti';
  static const listCutiUrl = basePegawaiUrl + 'list_cuti';

  static const pengajuanWFHUrl = basePegawaiUrl + 'pengajuan_wfh';
  static const listWFHUrl = basePegawaiUrl + 'list_wfh';

  static const pengajuanIzinSakitUrl = basePegawaiUrl + 'pengajuan_ijin_sakit';
  static const listIzinSakitUrl = basePegawaiUrl + 'list_ijin_sakit';

  static const pengajuanLemburUrl = basePegawaiUrl + 'pengajuan_lembur';
  static const listLemburUrl = basePegawaiUrl + 'list_lembur';

  static const pengajuanImbursmentUrl = basePegawaiUrl + 'pengajuan_imbursment';
  static const listImbursmentUrl = basePegawaiUrl + 'list_imbursment';

  static const listMeetingUrl = basePegawaiUrl + 'list_meeting';
  static const listIzinAllUrl = basePegawaiUrl + 'list_ijin';
}
